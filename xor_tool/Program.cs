﻿using System;
using System.IO;
using System.Windows.Forms;

namespace xor_tool
{
    class Program
    {
        static void Main(string[] args)
        {
            if ((args.Length != 1) && (args.Length != 3))
            {
                error();
            }

            if ((args.Length == 1) && (args[0].ToLower() == "-version"))
                Console.WriteLine(String.Format("{0}, written by {1} for catalinnc", Application.ProductName, Application.CompanyName));

            else if (args.Length == 3)
            {
                FileInfo fi1 = new FileInfo(args[0]);
                FileInfo fi2 = new FileInfo(args[1]);

                if (fi1.Length == 0 || fi2.Length == 0)
                    error();
                else if (fi1.Length != fi2.Length)
                    error();

                using (FileStream streamR1 = new FileStream(args[0], FileMode.Open, FileAccess.Read))
                using (FileStream streamR2 = new FileStream(args[1], FileMode.Open, FileAccess.Read))
                using (FileStream streamW = new FileStream(args[2], FileMode.Create, FileAccess.Write))
                using (BinaryReader reader1 = new BinaryReader(streamR1))
                using (BinaryReader reader2 = new BinaryReader(streamR2))
                using (BinaryWriter writer = new BinaryWriter(streamW))
                {
                    for (int i = 0; i < fi1.Length; i++)
                        writer.Write((byte)(reader1.ReadByte() ^ reader2.ReadByte()));
                }
            }

            else
                error();
        }

        private static void error()
        {
            Console.WriteLine("Usage: {0} value1.bin value2.bin result.bin" + Environment.NewLine, Application.ProductName);

            Environment.Exit(0);
        }
    }
}
